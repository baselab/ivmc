# Indecent virtual machine constructor

### Description

Virt-builder exists! But it is not so straightforward. Btw, this work uses virt-builder :)

Ubuntu-vm-builder exists too! Yes and it's nice, with Ubuntu.

This is Debian specific.
The dirty script uses some default values to built and install a basic Debian VM.

Some checks are performed, however, summary must be carefully read.
Options are parsed by bash 'getopts' which is not known to be bulletproof.

### Dependencies

* coreutils
* pwgen
* wamerican
* libvirt-clients
* libguestfs-tools
* qemu-utils
* a fully functional qemu/kvm environment

### Usage

Run it with (no needs to be root, must be in libvirt group):

    $ bash ivmc <options>

At least one option (and its argument) is required.
Everything else will go with defaults.

    OPTION  ARGUMENT     DESCRIPTION
    -w      <path>       Disk destination
    -t      <template>   Debian template
    -h      <hostname>   Vitual machine hostname
    -p      <password>   Root password
    -s      <size>       Disk size in GB (>=5)
    -n      <network>    Virtual network to attach
    -m      <size>       RAM size in MB
    -c      <number>     Number of processing units
    -f      <format>     Disk format
    -k      <layout>     Keyboard layout
    -x                   Doesn't install, stops after disk creation
    -d                   Recap default values
    -?                   Print this help

Examples:

    VM with defined hostname and password:
    $ bash ivmc -h foo -p bar

    VM with random hostname and random root password
    but attached to "net0" instead of default network:
    $ bash ivmc -n net0
